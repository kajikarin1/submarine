import pytest
from detection_unit.detect_submarine import detect_v2


@pytest.mark.parametrize("m_input, expected", [
    ([[0, 0, 0], [0, 0, 0], [0, 0, 0]], 0),
    ([[0, 1, 0], [0, 1, 0], [0, 0, 0]], 1),
    ([[1, 1, 1], [1, 1, 1], [1, 1, 1]], 1),
    ([[1, 1, 0], [1, 1, 0], [1, 1, 0]], 1),
    ([[1, 1, 1, 0, 0, 0, 0],
      [0, 0, 0, 0, 1, 0, 0],
      [0, 0, 0, 0, 1, 0, 1],
      [0, 0, 0, 0, 1, 0, 0],
      [0, 0, 0, 0, 0, 0, 0],
      [1, 1, 1, 0, 0, 0, 0],
      [1, 1, 1, 0, 0, 0, 0],
      [1, 1, 1, 0, 0, 0, 0]], 4),
])
def test_detect_v2(m_input, expected):
    assert detect_v2(m_input) == expected
