import numpy as np


def detect_v2(sea):
    """
    calculate how many submarines are in the sea
    :param sea: 2d matrix of 1 and 0 (1=occupied space)
    :return: number of detected submarines
    """
    sea = np.stack(sea)
    counter = 0

    for x in range(sea.shape[0]):
        for y in np.where(sea[x])[0]:
            if sea[x, y] == 1:
                upper_check = True if y != 0 and sea[x, y - 1] == 1 else False
                left_check = True if x != 0 and sea[x - 1, y] == 1 else False
                if upper_check or left_check:
                    continue
                counter += 1

    return counter


def detect_v1(sea):
    sea = np.stack(sea)
    counter = 0

    for x in range(sea.shape[0]):
        for y in range(sea.shape[1]):
            if sea[x, y] == 1:
                upper_check = True if y != 0 and sea[x, y - 1] == 1 else False
                left_check = True if x != 0 and sea[x - 1, y] == 1 else False
                if upper_check or left_check:
                    continue
                counter += 1

    return counter


def main():
    sea = [[1, 1, 1, 0, 0, 0, 0],
           [0, 0, 0, 0, 1, 0, 0],
           [0, 0, 0, 0, 1, 0, 1],
           [0, 0, 0, 0, 1, 0, 0],
           [0, 0, 0, 0, 0, 0, 0],
           [1, 1, 1, 0, 0, 0, 0],
           [1, 1, 1, 0, 0, 0, 0],
           [1, 1, 1, 0, 0, 0, 0]]

    submarines_sum = detect_v2(sea)
    print(submarines_sum)


if __name__ == '__main__':
    main()
